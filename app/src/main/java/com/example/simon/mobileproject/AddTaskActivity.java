package com.example.simon.mobileproject;

import android.content.Context;
import android.content.Intent;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

import com.activeandroid.ActiveAndroid;
import com.activeandroid.query.Select;

public class AddTaskActivity extends AppCompatActivity {

    private EditText subject;
    private EditText description;
    private Context context;
    private int position;
    private TaskList list;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ActiveAndroid.initialize(this);
        setContentView(R.layout.activity_add_task);
        context = this;
        readIntentExtra();



        list = new Select()
                .from(TaskList.class)
                .where("Id = ?", position)
                .executeSingle();




        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.save_task);

        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                subject = (EditText) findViewById(R.id.subject);
                description = (EditText) findViewById(R.id.description);

                Task t = new Task();
                t.setSubject(subject.getText().toString());
                t.setDescription(description.getText().toString());
                t.setTaskList(list);

                t.save();



                list.addTask(t);



                Intent i = new Intent(context, TaskOverviewActivity.class);
                i.putExtra("taskListId", position);
                startActivity(i);
            }
        });

    }

    private void readIntentExtra(){
        Bundle extras = getIntent().getExtras();
        position = extras.getInt("taskListId");
    }
}
