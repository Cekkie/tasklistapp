package com.example.simon.mobileproject;



import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by simon on 11/25/2015.
 */

@Table(name = "task_list")
public class TaskList extends Model {

    @Column(name="Name")
    private String name;

    public List<Task> tasks;


    public List<Task> tasks(){
        return getMany(Task.class, "TaskList");
    }

    public TaskList(){
        super();
    }

    public TaskList(String name){
        super();
        this.name = name;
    }

    public void setName(String name){
        this.name = name;
    }

    public String getName(){
        return name;
    }

    public void addTask(Task t){
        tasks().add(t);
    }

    public void removeTask(int id){
        for (Task t: tasks()) {
            if(t.getId() == id){
                tasks().remove(t);
            }
        }
    }

    public Task getTask(int id){
        for (Task t: tasks()) {
            if(t.getId() == id){
                return t;
            }
        }
        return null;
    }




}
