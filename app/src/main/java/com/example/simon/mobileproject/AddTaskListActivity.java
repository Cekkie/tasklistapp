package com.example.simon.mobileproject;

import android.content.Context;
import android.content.Intent;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

import com.activeandroid.ActiveAndroid;

public class AddTaskListActivity extends AppCompatActivity {
    private Context context;
    private EditText input;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        context = this;
        ActiveAndroid.initialize(this);
        setContentView(R.layout.activity_add_task_list);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.save_task_list);

        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                input = (EditText) findViewById(R.id.input);

                TaskList tl = new TaskList();
                tl.setName(input.getText().toString());
                tl.save();
                Intent i = new Intent(context, MainActivity.class);
                startActivity(i);
            }
        });

    }
}
