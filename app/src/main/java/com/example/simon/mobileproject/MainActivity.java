package com.example.simon.mobileproject;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.preference.PreferenceManager;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.activeandroid.ActiveAndroid;
import com.activeandroid.query.Select;
import com.google.gson.Gson;
import com.nhaarman.listviewanimations.appearance.simple.AlphaInAnimationAdapter;
import com.nhaarman.listviewanimations.appearance.simple.SwingBottomInAnimationAdapter;
import com.nhaarman.listviewanimations.appearance.simple.SwingLeftInAnimationAdapter;
import com.nhaarman.listviewanimations.appearance.simple.SwingRightInAnimationAdapter;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity{


    public TaskListManager tlm;
    ListView lv;
    Context context;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ActiveAndroid.initialize(this);
        setContentView(R.layout.activity_main);
        context = this;


        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fabAddList);

        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(context, AddTaskListActivity.class);
                startActivity(i);
            }
        });


        List<TaskList> lists = new Select()
                .from(TaskList.class)
                .execute();

        tlm = new TaskListManager(lists);


        lv = (ListView) findViewById(R.id.listview);

        TaskListAdapter adapter = new TaskListAdapter(this, tlm.gettTaskLists());

        SwingRightInAnimationAdapter animAdapter = new SwingRightInAnimationAdapter(adapter);
        animAdapter.setAbsListView(lv);
        lv.setAdapter(animAdapter);

    }

    public void onItemClick(int position){
        Intent intent = new Intent(this, TaskOverviewActivity.class);
        intent.putExtra("taskListId", position);
        startActivity(intent);
    }

    private void changeIntent(){
        Intent intent = new Intent(this, TaskOverviewActivity.class);
    }

    private void createEnvironment(){
        TaskList tl1 = new TaskList();
        tl1.setName("SCHOOL");
        tl1.save();


        Task t = new Task();
        t.setSubject("Mobile afwerken");
        t.setDescription("blbalblalbalglablla");
        t.setTaskList(tl1);
        t.save();
        //Task t2 = new Task("Maste", "beeg.com");
       // Task t3 = new Task("Kasse", "get better rank in league..");
       // Task t4 = new Task("Rave", "get wasted af....");
       // Task t5 = new Task("EAT", "dinner at 20 in leuven");
       // Task t6 = new Task("SLEEP", "optijd gaan slapen deze avond!");
       // Task t7 = new Task("REPEAT", "DO MORE!");


        tl1.addTask(t);
        //tl1.addTask(t2);
        //tl1.addTask(t3);
    //    tl1.addTask(t4);
      //  tl1.addTask(t5);
        //tl1.addTask(t6);
      //  tl1.addTask(t7);
    }



    @Override
    protected void onStop(){
        super.onStop();



    }

    @Override
    protected void onResume(){
        super.onResume();



    }

    @Override
    protected void onPause(){
        super.onPause();

    }
}
