package com.example.simon.mobileproject;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ListView;

import com.activeandroid.ActiveAndroid;
import com.activeandroid.query.Select;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;

public class TaskOverviewActivity extends AppCompatActivity {

    private TaskListManager tlm;
    private TaskList list;
    ListView lv;
    Context context;
    int position;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ActiveAndroid.initialize(this);
        setContentView(R.layout.activity_task_overview);
        context = this;
        readData();

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.add_task);

        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context, AddTaskActivity.class);
                intent.putExtra("taskListId", position);
                startActivity(intent);
            }
        });



             list = new Select()
                .from(TaskList.class)
                .where("Id = ?", position)
                .executeSingle();


        lv = (ListView)findViewById(R.id.taskListview);
        lv.setAdapter(new TaskAdapter(this, list.tasks()));

    }

    private void readData(){
        Bundle extras = getIntent().getExtras();
        position = extras.getInt("taskListId");
        list = new Select()
                .from(TaskList.class)
                .where("Id = ?", position)
                .executeSingle();


    }

    @Override
    protected void onResume(){
        super.onResume();

        readData();


    }
}
