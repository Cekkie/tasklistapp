package com.example.simon.mobileproject;


import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.res.Resources;
import android.text.Layout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import android.widget.Toast;

import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by simon on 11/25/2015.
 */
public class TaskAdapter extends BaseAdapter  {


    private Activity activity;
    private List<Task> data;
    private static LayoutInflater inflater = null;
    public Resources res;
    Task tempValues = null;
    int i = 0;

    public TaskAdapter(Activity a, List<Task> d){
        activity = a;
        data = d;

        inflater = (LayoutInflater)activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        if(data != null){
            if (data.size() <= 0) {
                return 0;
            }
            return data.size();

        }else{
            return 0;
        }
    }

    @Override
    public Object getItem(int position) {
        // TODO Auto-generated method stub
        return position;
    }

    @Override
    public long getItemId(int position) {
        // TODO Auto-generated method stub
        return position;
    }

    /************ Holder class die de inflated xml file elements bijhoud *********/
    public class Holder{
        TextView taskSubject;
        TextView taskDescription;
    }

    public View getView(final int position, View convertView, ViewGroup parent){
        View vi = convertView;

        Holder holder;

        if(convertView == null){
            /***** inflate tasklistitem.xml for each row ******/
            vi = inflater.inflate(R.layout.task_list_item, null);

            holder = new Holder();
            holder.taskSubject = (TextView) vi.findViewById(R.id.taskSubject);
            holder.taskDescription = (TextView) vi.findViewById(R.id.taskDescription);
            vi.setTag(holder);
        }else
            holder =(Holder)vi.getTag();

            if(data.size() <= 0){
                holder.taskSubject.setText("No Data");
            }
        else{
                tempValues = null;
                tempValues = (Task) data.get(position);

                holder.taskSubject.setText(tempValues.getSubject());
                holder.taskDescription.setText(tempValues.getDescription());
            }
        return vi;
    }



}
